/**
 * require couchbase to connect to cb
 */
var couchbase = require('couchbase');
var ViewQuery = couchbase.ViewQuery;

/**
 *
 * @param cbCred
 *            the config JSON
 * @constructor
 */
var CBConnector = function () {
//	this.connect();
};

/**
 * connects to couchbase
 *
 * @returns {exports.Connection|*}
 */
CBConnector.prototype.connect = function (bucketName) {

    console.log('try to create new connection to couchbase...');
    console.log(process.env.couchbaseHost);
    console.log(bucketName);
    console.log(process.env.couchbasePassword);

    var cluster = new couchbase.Cluster(process.env.couchbaseHost);
    cb = cluster.openBucket(bucketName, process.env.couchbasePassword);

    console.log(cb);

    console.log('opened cb bucket -> ' + bucketName);
    return cb;
};


CBConnector.prototype.create = function (cb, docKey, docContent, callback) {

    cb.insert(docKey, docContent, function(err, res) {
        if(err){
            console.error(err);
            callback(err, null);
        } else {
            console.log('successfully created new cb document...',  res);
            callback(null, res);
        }
    });
};


CBConnector.prototype.update = function (cb, docKey, docContent, callback) {

    cb.replace(docKey, docContent, function(err, res) {
        if(err){
            console.error(err);
            callback(err, null);
        } else {
            console.log('successfully updated cb document...',  res);
            callback(null, res);
        }
    });
};


CBConnector.prototype.delete = function (cb, docKey, callback) {

    cb.remove(docKey, function(err, res) {
        if(err){
            console.error(err);
            callback(err, null);
        } else {
            console.log('successfully deleted cb document...',  res);
            callback(null, res);
        }
    });
};



/**
 * execute a view query for a given doc key
 */
CBConnector.prototype.viewQuery = function (cb, designDocumentName, viewName, viewKey, callback) {
    var query = ViewQuery.from(designDocumentName, viewName).key(viewKey).stale(ViewQuery.Update.BEFORE);
    cb.query(query, function (err, results) {
        if (err) {

            console.error(err);

            callback(err, null);
        } else {
            var result = [];
            if (results && results.length > 0) {
                for (var i = 0; i < results.length; ++i) {
                    result.push(results[i].value);
                }
            }
            callback(null, result);
        }
    });
};


CBConnector.prototype.viewQueryAll = function (cb, designDocumentName, viewName, callback) {
    var query = ViewQuery.from(designDocumentName, viewName).stale(ViewQuery.Update.BEFORE);
    cb.query(query, function (err, results) {
        if (err) {
            console.error(err);
            callback(err, null);
        } else {
            var result = [];
            if (results && results.length > 0) {
                for (var i = 0; i < results.length; ++i) {
                    result.push(results[i].value);
                }
            }
            callback(null, result);
        }
    });
};


/**
 * execute a view query for a given doc key range
 */
CBConnector.prototype.rangeQuery = function (cb, designDocumentName, viewName, viewKeyStart, viewKeyEnd, groupLevel, reduce, callback) {

    console.log('CBConnector.prototype.rangeQuery -> got groupLevel: ', groupLevel, typeof(groupLevel));


    var query = ViewQuery.from(designDocumentName, viewName).
        range(viewKeyStart, viewKeyEnd, false).
        stale(ViewQuery.Update.BEFORE);

    if(reduce){
        query.reduce(parseInt(reduce) === 1 ? true : false);
    }

    if(groupLevel && parseInt(reduce) === 1){
        query.group_level(groupLevel);
    }



    console.log('current query object: ', query);

    cb.query(query, function (err, results) {
        if (err) {
            console.error(err);
            callback(err, null);
        } else {
            var result = [];
            if (results && results.length > 0) {
                for (var i = 0; i < results.length; ++i) {
                    result.push(results[i].value);
                }
            }

            console.log('found the following result: ', result);

            callback(null, result);
        }
    });
};

module.exports = CBConnector;