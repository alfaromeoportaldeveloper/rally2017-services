var CBConnector = require(__dirname + '/util/cb-connector');
var CB = new CBConnector();


var callback;

module.exports.getParticipants = function (request, context, callback) {
    this.callback = callback;

    console.log('called getParticipants() ...', JSON.stringify(request));

    // try to connect to couchbase
    var cb = CB.connect(process.env.couchbaseBucket);

    CB.viewQueryAll(cb, process.env.designDocumentName, process.env.listParticipantsView,
        function (err, results) {
            if (err) {
                console.error(err);
                // close connection to couchbase
                cb.disconnect();
                errorHandler(err, 'Could not query participants data!');
            } else {
                console.log('got participants data from couchbase query...');
                console.log(results);

                // close connection to couchbase
                cb.disconnect();
                responseHandler(results);
            }
        }
    );
};


module.exports.addParticipant = function (request, context, callback) {
    this.callback = callback;

    console.log('called addParticipant() ...', request);

    // try to connect to couchbase
    var cb = CB.connect(process.env.couchbaseBucket);

    var participant = JSON.parse(request.body);

    var documentKey = 'CAR_2017_' + participant.car.licensePlatePart1 + '_' + participant.car.licensePlatePart2 + '_' +  participant.car.licensePlatePart3;

    CB.create(cb, documentKey, participant,
        function(err, result){
            if (err) {
                console.error(err);
                // close connection to couchbase
                cb.disconnect();
                errorHandler(err, 'Could not add new participant!');
            } else {
                console.log('successfully saved new participant...');
                console.log(result);

                // close connection to couchbase
                cb.disconnect();
                responseHandler(result);
            }
        }
    );
};

/**
 * methode to update complete participant object (start, stop timer, reset timer aso)
 * @param request
 * @param context
 * @param callback
 */
module.exports.updateParticipant = function (request, context, callback) {
    this.callback = callback;
    console.log('called updateParticipant() ...', request);

    // try to connect to couchbase
    var cb = CB.connect(process.env.couchbaseBucket);


    var participant = JSON.parse(request.body);

    var documentKey = 'CAR_2017_' + participant.car.licensePlatePart1 + '_' + participant.car.licensePlatePart2 + '_' +  participant.car.licensePlatePart3;


    CB.update(cb, documentKey, participant,
        function(err, result){
            if (err) {
                console.error(err);
                // close connection to couchbase
                cb.disconnect();
                errorHandler(err, 'Could not update participant!');
            } else {
                console.log('successfully updated participant...');
                console.log(result);

                // close connection to couchbase
                cb.disconnect();
                responseHandler(result);
            }
        }
    );
};


/**
 * methode to delete complete participant object
 * @param request
 * @param context
 * @param callback
 */
module.exports.deleteParticipant = function (request, context, callback) {
    this.callback = callback;
    console.log('called deleteParticipant() ...', request);

    // try to connect to couchbase
    var cb = CB.connect(process.env.couchbaseBucket);


    var participant = JSON.parse(request.body);

    var documentKey = request.pathParameters.participantDocKey;


    CB.delete(cb, documentKey,
        function(err, result){
            if (err) {
                console.error(err);
                // close connection to couchbase
                cb.disconnect();
                errorHandler(err, 'Could not delete participant!');
            } else {
                console.log('successfully deleted participant...');
                console.log(result);

                // close connection to couchbase
                cb.disconnect();
                responseHandler(result);
            }
        }
    );
};


function errorHandler(err, message) {
    var response = {
        statusCode: 400,
        body: JSON.stringify({'message': message})
    };

    console.log(response);

    this.callback(err, response);
};

function successHandler(message) {
    var response = {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Origin": "*", // Required for CORS support to work
            "Access-Control-Allow-Credentials": true // Required for cookies, authorization headers with HTTPS
        },
        body: JSON.stringify({'message': message})
    };

    console.log(response);


    this.callback(null, response);
};


function responseHandler(responseObject) {
    var response = {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Origin": "*", // Required for CORS support to work
            "Access-Control-Allow-Credentials": true // Required for cookies, authorization headers with HTTPS
        },
        body: JSON.stringify(responseObject)
    };

    console.log(response);


    this.callback(null, response);
};